﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_Obstruction : MonoBehaviour {

    public enum Type
    {
        RED,
        GREEN,
        BLUE
    }

    public Type currentType;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        switch (currentType)
        {
            case (Type.RED):
                {
                    gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
                    break;
                }
            case (Type.BLUE):
                {
                    gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.blue);
                    break;
                }
            case (Type.GREEN):
                {
                    gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.green);
                    break;
                }
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.tag == "Projectile")
        {
            switch(currentType)
            {
                case (Type.RED):
                    {
                        if (other.gameObject.GetComponent<SCR_Projectile>().currentType == SCR_Projectile.ProjType.BLUE)
                            Destroy(gameObject);
                        break;
                    }
                case (Type.BLUE):
                    {

                        if (other.gameObject.GetComponent<SCR_Projectile>().currentType == SCR_Projectile.ProjType.GREEN)
                            Destroy(gameObject);
                        break;
                    }
                case (Type.GREEN):
                    {
                        if (other.gameObject.GetComponent<SCR_Projectile>().currentType == SCR_Projectile.ProjType.RED)
                            Destroy(gameObject);
                        break;
                    }
            }

            
        }
    }
}
