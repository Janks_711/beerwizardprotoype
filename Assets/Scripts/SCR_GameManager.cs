﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SCR_GameManager : MonoBehaviour {

    public GameObject playerObj;

    public Text drunkText;

	// Use this for initialization
	void Start () {
        playerObj = GameObject.Find("PREF_Player");

        if (playerObj == null)
            Debug.Log("Player Not Found");

	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();

        drunkText.text = playerObj.GetComponent<SCR_Player>().drunkenness.ToString();

       

    }
}
