﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SCR_LevelExit : MonoBehaviour {

    public string levelName = "Unkown";

	// Use this for initialization
	void Start () {
        if (levelName == "Unknown")
            Debug.Log("Enter Level Name");
	}
	
	// Update is called once per frame
	void Update () {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Collider>().tag == "Player")
        {
            SceneManager.LoadScene(levelName);
            Debug.Log("Next Level Please");
        }
    }
}
