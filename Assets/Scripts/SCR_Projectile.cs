﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_Projectile : MonoBehaviour {

    public enum ProjType
    {
        DEFAULT,
        RED,
        BLUE,
        GREEN, 
    }


    public float speed = 50;
    public ProjType currentType;

    public GameObject player;

	// Use this for initialization
	void Start () {
        currentType = ProjType.DEFAULT;
        player = GameObject.Find("PREF_Player");
    }
	
	// Update is called once per frame
	void Update () {

        this.transform.Translate(Vector3.forward * speed * Time.deltaTime);

        switch(player.GetComponent<SCR_Player>().currentType)
        {
            case (SCR_Player.damageType.DMGRED):
                {
                    currentType = ProjType.RED;
                    break;
                }
            case (SCR_Player.damageType.DMGBLUE):
                {
                    currentType = ProjType.BLUE;
                    break;
                }
            case (SCR_Player.damageType.DMGGREEN):
                {
                    currentType = ProjType.GREEN;
                    break;
                }
        }


        switch(currentType)
        {
            case (ProjType.RED):
                {
                    gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
                    break;
                }
            case (ProjType.BLUE):
                {
                    gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.blue);
                    break;
                }
            case (ProjType.GREEN):
                {
                    gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.green);
                    break;
                }
           default:
                {
                    gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.white);
                    break;
                }
        }

	}

    public void OnCollisionEnter(Collision other)
    {
        if (other.collider.tag == "Player")
        {
            Physics.IgnoreCollision(this.GetComponent<Collider>(), other.collider);
            
        }

        Destroy(gameObject);

    }
}
