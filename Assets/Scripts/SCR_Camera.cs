﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_Camera : MonoBehaviour {

    public GameObject player;
    public float speed = 10.0f;
    private Vector3 boom;

	// Use this for initialization
	void Start () {
        if (player == null)
            Debug.Log("Need to attach player");

        boom = this.transform.position - player.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 targetPos = boom + player.transform.position;
        this.transform.position = Vector3.Lerp(transform.position, targetPos, speed * Time.deltaTime);
	}
}
