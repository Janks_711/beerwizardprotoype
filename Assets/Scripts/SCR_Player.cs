﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_Player : MonoBehaviour {

    public enum damageType {
        DMGDEFAULT,
        DMGRED,
        DMGBLUE,
        DMGGREEN
    }


    //Character Vitals
    public int health = 100;
    public float drunkenness = 0;
    public float speed = 100;
    public float maxSpeed;
    public damageType currentType = damageType.DMGDEFAULT;

    // RigidBody
    public Rigidbody myRigid;
    bool grounded = true;

    public GameObject firePoint;
    public GameObject projectile;

	// Use this for initialization
	void Start () {
        myRigid = GetComponent<Rigidbody>();
        currentType = damageType.DMGDEFAULT;

        maxSpeed = speed;

        firePoint = GameObject.Find("FirePoint");
        
    }
	
	// Update is called once per frame
	void Update () {
        Move();
        
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            currentType = damageType.DMGRED;
            incDrunk(25);
            Debug.Log("Colour should be red");
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            currentType = damageType.DMGBLUE;
            incDrunk(25);
            Debug.Log("Colour should be red");
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            currentType = damageType.DMGGREEN;
            incDrunk(25);
            Debug.Log("Colour should be red");
        }

        // Set properties for each damage type
        switch (currentType)
        {
            case damageType.DMGDEFAULT:
                {
                    this.GetComponent<Renderer>().material.SetColor("_Color", Color.white);
                    break;
                }
            case damageType.DMGRED:
                {
                    this.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
                    projectile.GetComponent<SCR_Projectile>().currentType = SCR_Projectile.ProjType.RED;
                    break;
                }
            case damageType.DMGBLUE:
                {
                    this.GetComponent<Renderer>().material.SetColor("_Color", Color.blue);
                    projectile.GetComponent<SCR_Projectile>().currentType = SCR_Projectile.ProjType.BLUE;
                    break;
                }
            case damageType.DMGGREEN:
                {
                    this.GetComponent<Renderer>().material.SetColor("_Color", Color.green);
                    projectile.GetComponent<SCR_Projectile>().currentType = SCR_Projectile.ProjType.GREEN;
                    break;
                }
        }

        Fire();

        if (drunkenness > 0)
        {
            drunkenness -= 1.5f * Time.deltaTime;
            speed = (maxSpeed * 20) / drunkenness;
        }
            
	}

    //Define movement for the chracter
    private void Move()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(-vertical, 0.0f, horizontal);

        transform.Translate(movement * speed * Time.deltaTime, Space.World);

        if(movement != Vector3.zero)
            transform.rotation = Quaternion.LookRotation(movement);

        //jump

        if (this.transform.position.y > 1.01)
            grounded = false;
        else
            grounded = true;

        if (grounded == true && Input.GetKey(KeyCode.Space))
        {
            this.transform.Translate(new Vector3(0, 10.0f, 0) * speed * Time.deltaTime, Space.World);
        }

    }

    public void Fire()
    {
        if(Input.GetKeyDown(KeyCode.E))
        {

            Instantiate(projectile, firePoint.transform.position, transform.rotation);
        }
    }

    public void incDrunk(float drunkToApply)
    {
        drunkenness += drunkToApply;
    }

}
