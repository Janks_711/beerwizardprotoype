﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_DoorScript : MonoBehaviour {

    public float speed = 0.005f;

    private bool closed = false;

	// Use this for initialization
	void Start () {
        closed = false;
	}
	
	// Update is called once per frame
	void Update () {

        if (closed == false)
            this.transform.position -= new Vector3(0.0f, speed, 0.0f);
        else
            Debug.Log("Closed, You're Dead");
	}

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.tag == "Floor")
            closed = true;
    }
}
